Support and suggestions @www.the-pwg.eu

Config files:
ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
!!! Deactivate steam cloud !!!

Files to edit:
!!! a_personal.cfg --> you MUST change Resolution / Mouse / and other personal stuff !!!
(look for your old values in the config.cfg from your backup)

Optional files: just copy in your \left4dead2\addons folder
simplemenu.vpk
zonegui.vpk
guiside.vpk

Installation:
!!! BACKUP YOUR OLD config.cfg + autoexec.cfg !!!

copy the folders from the unpacked zip (\backup_your_files_before_overwrite\left4dead2
to your \Steam\steamapps\common\left 4 dead 2\left4dead2 folder and overwrite existing files when asked...

(YOU MUST MODIFY a_personal.cfg: change to your resolution and remove the // in front)

//mat_setvideomode 1680 1050 1             //# mat_setvideomode width height mode
//mat_setvideomode 1680 1050 0             //# Mode: 0. Fullscreen 1. Window Mode

All bindings must be done ingame in the normal keyboard configuration.


Startoptions: (has to be changed to your system)
ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
-console -noforcemaccel -noforcemparms -noipx -nojoy -novid -threads 4 -heapsize 1572864 +dsp_enhance_stereo 1 +mat_motion_blur_percent_of_screen_max 0
General help and support:
http://the-pwg.eu/forum/viewtopic.php?f=4&t=174

Manual:
+mat_motion_blur_percent_of_screen_max 0 - If Shader above Medium it will force motion blur off
+dsp_enhance_stereo 1 - Enhance stereo sound quality
+hostname             - Sets the default hostname when hosting.
+sv_lan 1             - Connecting to LAN games
-console              - Enables developer console
-noforcemaccel        - Forces desktop mouse acceleration settings
-noforcemparms        - Forces desktop mouse button configuration
-noipx                - Disables IPX network detection, local LAN (slight boot time improvement; you may need to remove this for LAN parties)
-nojoy                - Disables detection of 3rd party gamepads and controllers (slight boot time improvement)
-novid                - Disables the intro movie, speeds up game on start-up
-lv                   - Low Violence settings - makes zombie dissapear instantly at death (alot of fps boost)
-threads              - Makes use of all your cpu cores (if you have a quadcore, use 4, dualcore 2, single core just omit this or use 1)
-noaafonts            - Disables Anti-Aliasing of Screen Fonts.
-refresh 75           - Changes the Hertz for monitors HL2 Engine. CRT 60-100 85=Common LCD 60-75 72=Common
-heapsize 1572864     - Allocates RAM for the game; see table below for calculating optimal values
-high                 - Forces high CPU priority (not using this, may be good on lowend computers but worst on highend computers)

Heapsize Setting
ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
Modify the heapsize command to allocate 50% of the RAM you have
in your computer (NOT video card memory!) in bytes.

Formula = RAM (in GB) * 1024 * 1024 / 2

512MB   -heapsize 262144
1GB     -heapsize 524288
2GB     -heapsize 1048576
3GB(max)-heapsize 1572864


When Windows Power Options are set to High Performance, specifically Minimum and Max processor state set to 100% the game glitches
changing the Maximum to 90% and the Minimum to 5% (the defaults for balanced), corrects the problem.

http://www.youtube.com/watch?v=YE9okNuL8FY

hf